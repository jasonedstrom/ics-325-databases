<?php
include 'data/functions.php';
    $output_form = true;
        $success = null;
    session_start();
    if (isset ($_SESSION['administrator'])){
    if ($_SESSION['administrator']){
        if (isset($_GET['categoryAdd'])){
        $catName = filter_input(INPUT_GET, 'catName', FILTER_SANITIZE_SPECIAL_CHARS);
        $catDescription = filter_input(INPUT_GET, 'catDescription', FILTER_SANITIZE_SPECIAL_CHARS);

        $sql = "Insert into category VALUES ('default', '$catName', '$catDescription');";

         $success = SQLquery($sql);


        }

        if (isset($_POST['productAdd'])){
            $product_name = filter_input(INPUT_POST, 'product_name', FILTER_SANITIZE_SPECIAL_CHARS);
            $product_category = filter_input(INPUT_POST, 'product_category', FILTER_SANITIZE_SPECIAL_CHARS);
            $product_cost = filter_input(INPUT_POST, 'product_cost', FILTER_SANITIZE_SPECIAL_CHARS);
            $product_hours = filter_input(INPUT_POST, 'product_hours', FILTER_SANITIZE_SPECIAL_CHARS);
            $product_description = filter_input(INPUT_POST, 'product_description', FILTER_SANITIZE_SPECIAL_CHARS);
            $product_image = $_FILES['product_image']['name'];
            $product_image = GenerateSafeFileName($product_image);
            $sql = "Insert into product VALUES ('Default','$product_name','$product_category', '$product_cost', '$product_hours', '$product_description', '$product_image');";
            $success = SQLquery($sql);
            uploadPicture();
        }

        $sql = "Select category from category;";
        $result = SQLquery($sql);
        $numRows = $result ->num_rows;
        
        
        
        }
    }
?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css"  type="text/css" media="screen" />
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
        if (isset($_SESSION['loggedIn'])){

            if ($_SESSION['loggedIn']){
                if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))){
                    session_destroy();
                } else {
                    ?>
                    <div id="welcome">Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a href="logout.php" class="buttons">Logout</a>
                        <?php
                        if (isset($_SESSION['administrator'])){
                            if ($_SESSION['administrator'] == true){
                                ?>
                                <a href="admin.php" class="buttons">Admin</a>
                            <?php
                            }
                        }
                        ?>

                    </div>

                <?php
                }
            } else{
                ?>
                <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
            <?php
            }
        }else{
            ?>
            <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
        <?php
        }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php"   class="active" onclick="Javascript:window.location.assign('index.php');">Home</a></li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar"><div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            </ul>
        </div>
        <div id="content_inside_main">
            <? if (isset ($_SESSION['administrator'])){
               if ($_SESSION['administrator']){

                   if ($success){
                       echo "Category/Product Added. <br>";
                   } else if ($success == null){
                            echo "";
                   }else {
                       echo "Category/Product Could Not Be Added. <br>";
                   }
             ?>

            <h3>Add a Category</h3>
            <form method="get" action="add.php" >
               <label for="catName">Category Name: </label><input type="text" name="catName"><br>
                <label for="catDescription">Category Description: </label><textarea name="catDescription"></textarea><br>

                <input type="submit" name="categoryAdd" value="Add Category">
            </form><br>

            <h3>Add a Product</h3>
                   <form method="post" action="add.php" enctype="multipart/form-data" >
                       <label for="product_name">Product Name: </label><input type="text" name="product_name"><br>
                       <label for="product_category">Category Name: </label>
                       <select name="product_category">
                           <?php

                           if($numRows > 0){
                           while ($row =  $result->fetch_assoc()){

                               $item = $row['category'];

                               echo "<option value='".$item."'>$item</option>";

                               }
                              }
                           ?>
                       </select><br>
                       <label for="product_cost">Product Cost: </label><input type="text" name="product_cost"><br>
                       <label for="product_hours">Hours for Cost : </label><input type="text" name="product_hours"><br>
                       <label for="product_description">Product Description: </label><textarea name="product_description"></textarea><br>
                       <label for="product_image">Product Image: </label><input type="file" name="product_image"><br>
                       <input type="submit" name="productAdd" value="Add Product">
                   </form><br>

            <?php
               }
            } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }
            ?>

			</div>	
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>