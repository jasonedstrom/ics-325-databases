<?php
    $output_form = true;

    session_start();
    if (isset ($_POST['submit'])){
        $username = trim(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_SPECIAL_CHARS));
        $password = trim(filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS));

        $host = "localhost";
        $db_username = "agentmoobius";
        $db_password = "Moobie1234!";
        $db_name = "ICS325";

        if (empty($username) === true || empty($password) === true){
            $errors[]= "Please fill in all the fields";
        } else {
            $output_form = false;
        }

    if (!$output_form){

        $user_login = array('Username' => $username, 'Password' => $password);

        $db = new mysqli($host, $db_username, $db_password, $db_name);


        $connection_error = $db->connect_error;
        if ($connection_error != null){
            $errors[] = "Errors creating database connection";
            exit;
        }


        $sql = "Select * from users where Username='$user_login[Username]' and Password=sha1('$user_login[Password]');";

        $result = $db->query($sql);

        if (!$result){
            echo( "<p>Unable to query database at this time.</p>" );
            exit();

        }

        $numRows = $result ->num_rows;
        //$db ->close();
        if($numRows > 0){
            $row =  $result->fetch_assoc();
            $_SESSION['loggedIn'] = true ;
            $_SESSION['FirstName'] = $row['FirstName'];
            $_SESSION['LastName'] = $row['LastName'];

            if ($row['administrator']){
                $_SESSION['administrator'] = true;
            }

        } else {
            $errors[] = "Username or password is incorrect.  Try again.";
        }

        }
    }



?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css"  type="text/css" media="screen" />
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])){

            if ($_SESSION['loggedIn']){
                if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))){
                   session_destroy();
                } else {
            ?>
                <div id="welcome">Welcome,  <?php echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a href="logout.php" class="buttons">Logout</a></div>

            <?php
                }
            } else{
        ?>
        <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
        <?php
            }
            }else{
        ?>
        <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
        <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php"   class="active" onclick="Javascript:window.location.assign('index.php');">Home</a></li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar"><div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            </ul>
        </div>
        <div id="content_inside_main">

            <?php



            if (empty($errors) === false) {
                echo '<ul>';
                foreach ($errors as $error) {
                    echo '<li class = "error">', $error, '</li>';
                }
                echo '</ul>';


                $output_form = true;
            }

                if($output_form){
            ?>
				<form method = "post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <table class="layout">
                    <tr><td class="layout"><label for="username">Username: </label></td><td class="layout"><input type="text" name="username" <?php if (isset($_POST['username']) === true) {echo 'value="', strip_tags($_POST['username']), '"';} ?>></td> </tr>
                    <tr><td class="layout"><label for="password">Password: </label></td><td class="layout"><input type="password" name="password" <?php if (isset($_POST['password']) === true) {echo 'value="', strip_tags($_POST['password']), '"';} ?>></td> </tr>
                    </table>

                    <input type="submit" name = 'submit' id="submit" value="Login"><input type="button" value="Register" onclick="location.href = 'register.php';">
				</form>

            <?php
                } else{

                    echo "Logged in as " . $_SESSION['FirstName']. " ". $_SESSION['LastName'];


                    if ($_SESSION['administrator']){

                        echo "<script>setTimeout(function(){window.location.href = 'users.php';}, 4000);</script>";

                    } else{
                    echo "<script>setTimeout(function(){window.location.href = 'index.php';}, 4000);</script>";
                    }
                }
            ?>
			</div>	
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>