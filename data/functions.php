<?php
/**
 * Created by PhpStorm.
 * User: jasonedstrom
 * Date: 4/7/14
 * Time: 12:15 AM
 */


$host = "localhost";
$db_username = "agentmoobius";
$db_password = "Moobie1234!";
$db_name = "ICS325";

function SQLquery($query){


    $db = new mysqli($GLOBALS['host'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);


    $connection_error = $db->connect_error;
    if ($connection_error != null){
        $errors[] = "Errors creating database connection";
        exit;
    }




    $result = $db->query($query);

    if (!$result){
        echo( "<p>Unable to query database at this time.</p>" );
        exit();

    } else {
        $db->close();
        return $result;
    }





}

function uploadPicture(){
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    $temp = explode(".", $_FILES["product_image"]["name"]);
    $extension = end($temp);
    if ((($_FILES["product_image"]["type"] == "image/gif")
            || ($_FILES["product_image"]["type"] == "image/jpeg")
            || ($_FILES["product_image"]["type"] == "image/jpg")
            || ($_FILES["product_image"]["type"] == "image/pjpeg")
            || ($_FILES["product_image"]["type"] == "image/x-png")
            || ($_FILES["product_image"]["type"] == "image/png"))
        //&& ($_FILES["product_image"]["size"] < 20000)
        && in_array($extension, $allowedExts))
    {
        if ($_FILES["product_image"]["error"] > 0)
        {
            echo "Return Code: " . $_FILES["product_image"]["error"] . "<br>";
        }
        else
        {


            if (file_exists("upload/" . GenerateSafeFileName($_FILES["product_image"]["name"])))
            {
                //echo $_FILES["product_image"]["name"] . " already exists. ";
            }
            else
            {
                move_uploaded_file($_FILES["product_image"]["tmp_name"],
                    "upload/" . GenerateSafeFileName($_FILES["product_image"]["name"]));
                //echo "Stored in: " . "upload/" . $_FILES["product_image"]["name"];
            }
        }
    }
    else
    {
        echo "Invalid file";
    }

}

function GenerateSafeFileName($filename) {
    $filename = strtolower($filename);
    $filename = str_replace("#","_",$filename);
    $filename = str_replace(" ","_",$filename);
    $filename = str_replace("'","",$filename);
    $filename = str_replace('"',"",$filename);
    $filename = str_replace("__","_",$filename);
    $filename = str_replace("&","and",$filename);
    $filename = str_replace("/","_",$filename);
    $filename = str_replace("\\","_",$filename);
   $filename = str_replace("?","",$filename);
   return $filename;
}


?>