<?php
session_start();
include "data/functions.php";

$sql = "Select category from category;";
$result = SQLquery($sql);
$numRows = $result ->num_rows;

$output_results = false;

if (isset($_GET['select'])){

    $product_category = filter_input(INPUT_GET, 'product_category', FILTER_SANITIZE_SPECIAL_CHARS);

    $sql = "Select * from product where product_category = '$product_category'";

    $product_results = SQLquery($sql);
    $product_numRows = $product_results -> num_rows;
    $output_results = true;
}
?>

<!DOCTYPE html>

<head>
	<title>Dev Your Team</title>
	<link rel="stylesheet" href="styles/styles.css"  type="text/css"/>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
    <script src="scripts/scripts.js" type="text/javascript"></script>

    <script>


    </script>
</head>

<body>
	<div id="header">
		<div id="header_inside">
            <?php
            if (isset($_SESSION['loggedIn'])){

                if ($_SESSION['loggedIn']){
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))){
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a href="logout.php" class="buttons">Logout</a>
                            <?php
                            if (isset($_SESSION['administrator'])){
                                if ($_SESSION['administrator'] == true){
                                    ?>
                                    <a href="admin.php" class="buttons">Admin</a>
                                <?php
                                }
                            }
                            ?>

                        </div>

                    <?php
                    }
                } else{
                    ?>
                    <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
                <?php
                }
            }else{
                ?>
                <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
            <?php
            }
            ?>
            <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
			<ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
                <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
				<li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
				<li><a href="team.php" onclick="Javascript:window.location.assign('team.php');">Team</a></li>
				<li><a href="project.php" class="active" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
				<li><a href="index.php"   onclick="Javascript:window.location.assign('index.php');">Home</a></li>
			</ul>		
		</div>
	</div>
	<div id="content">
		<div id="content_inside">
			<div id="content_inside_sidebar"><div id="clock">&nbsp;</div>
		
				<h2>Project</h2>
				<ul>
                    <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
				</ul>
				<h2>Team</h2>
				<ul>
                    <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
				</ul>
				<h2>Ideas</h2>
				<ul>
                    <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
				</ul>
                <h2>Developers</h2>
                <ul>
                    <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                    <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
                </ul>

				<h2>Other</h2>
				<ul>
                    <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
				</ul>
			</div>
			<div id="content_inside_main">
                <form>
                    <select name="product_category">
                        <?php

                        if($numRows > 0){
                            while ($row =  $result->fetch_assoc()){

                                $item = $row['category'];

                                echo "<option value='".$item."'>$item</option>";

                            }
                        }
                        ?>
                    </select><input type="submit" name="select" value="Select"><br>
                </form>

                <?php
                    if ($output_results){


                ?>

                        <h3><? print $product_category ?></h3>
                        <table class="product">
                            <thead>
                            <th>Picture</th><th>Item Name</th><th>Description</th><th>Project Hours</th><th>Price</th>
                            </thead>
                            <tbody>

                            <?php

                            if ($product_numRows > 0){
                                while ($product_rows = $product_results-> fetch_assoc()){


                           echo  "<tr><td><img class='productimg' src='upload/".$product_rows['product_image']."'></td><td><a href='product.php?id=".$product_rows['id']."'>".$product_rows['product_name']."</a></td><td>".$product_rows['product_description']."</td><td>".$product_rows['product_hours']."</td><td>".$product_rows['product_cost']."</td></tr>";


                                }
                            }
                            ?>
                            </tbody>
                        </table>




                <?php
                    }
                ?>

           <!-- <h3>Graphic Design</h3>
                <table class="product">
                   <thead>
                         <th>Picture</th><th>Item Name</th><th>Description</th><th>Project Hours</th><th>Price</th>
                   </thead>
                    <tbody>
                    <tr><td><a id="product1"><img class="productimg" src="http://s.w.org/about/images/logos/wordpress-logo-notext-rgb.png"></a></td><td><a href="wordpress.html">Wordpress Theme</a></td><td>Make it all pretty!</td><td>40 hrs</td><td> $150</td></tr>
                    <tr><td><a id="product2"><img class="productimg" src="http://prolificphoto.com/wp-content/uploads/2013/01/Adobe-Photoshop-Logo.png"></a></td><td><a href="photoshop.html">Photoshop Guru</a></td><td>Make it all pretty!</td><td>40 hrs</td><td> $150</td></tr>

                    </tbody>
                </table>
                <br>
                <table class="product">
                    <thead>
                    <th>Picture</th><th>Item Name</th><th>Description</th><th>Project Hours</th><th>Price</th>
                    </thead>
                    <tbody>
                    <tr><td><img class="productimg" src="http://www.2expertsdesign.com/wp-content/uploads/2012/03/Html5LogoIcon.jpg"></td><td>HTML5</td><td>Cutting edge!</td><td>25 hrs</td><td>$200</td></tr>
                    <tr><td><img class="productimg" src="http://joelsaupe.com/wp-content/uploads/2014/01/css3.png"></td><td>CSS3</td><td>Cutting edge!</td><td>25 hrs</td><td>$200</td></tr>
                    </tbody>
                </table>-->



			</div>	
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>