<?php
/**
 * Created by PhpStorm.
 * User: jasonedstrom
 * Date: 2/23/14
 * Time: 12:44 PM
 */
$output_form = true;
$Administrator = 0;

if(isset($_POST['submit'])){
    //Sanitizing inputs
    //$_POST = sanitize($_POST);

    /*$FName = $_POST['FName'];
    $LName = $_POST['LName'];
    $Email = $_POST['Email'];
    $Birthday = $_POST['Birthday'];
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $Comments = $_POST['Comments'];*/

    $FName = filter_input(INPUT_POST, 'FName', FILTER_SANITIZE_SPECIAL_CHARS);
    $LName = filter_input(INPUT_POST, 'LName', FILTER_SANITIZE_SPECIAL_CHARS);
    $Email = filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_SPECIAL_CHARS);
    $Birthday = filter_input(INPUT_POST, 'Birthday', FILTER_SANITIZE_SPECIAL_CHARS);
    $Username = filter_input(INPUT_POST, 'Username', FILTER_SANITIZE_SPECIAL_CHARS);
    $Password = filter_input(INPUT_POST, 'Password', FILTER_SANITIZE_SPECIAL_CHARS);
    $Comments = filter_input(INPUT_POST, 'Comments', FILTER_SANITIZE_SPECIAL_CHARS);


    $host = "localhost";
    $db_username = "agentmoobius";
    $db_password = "Moobie1234!";
    $db_name = "ICS325";

    if (isset($_POST['TC'])){
    $TCAgree = $_POST['TC'];
    }
 if (empty($FName) === true || empty($LName) === true || empty($Email) === true || empty($Username) === true || empty($Password) === true || $Birthday == "mm/dd/yyyy"){
     $errors[]= "Please fill in all the fields";
 } else if ($TCAgree != "agree"){
     $errors = "Please agree to the terms and conditions";
 } else {
     $output_form = false;


 }


    if (!$output_form){

        $user_submission = array('FirstName' => $FName, 'LastName' => $LName, 'Email' => $Email, 'Birthday' => $Birthday, 'Username' => $Username, 'Password' => "$Password", 'Comments' => $Comments, 'Administrator' => $Administrator);

        $db = new mysqli($host, $db_username, $db_password, $db_name);


        $connection_error = $db->connect_error;
        if ($connection_error != null){
            $errors[] = "Errors creating database connection";
            exit;
        }

        $sql = "Insert into users VALUES ('Default', " .
            "'$user_submission[FirstName]', " .
            "'$user_submission[LastName]', " .
            "'$user_submission[Email]', " .
            "'$user_submission[Birthday]',  " .
            "'$user_submission[Username]', " .
            "sha1('$user_submission[Password]'), " .
            "'$user_submission[Comments]', " .
            "'$user_submission[Administrator]');";

        $success = $db -> query($sql);

        //$db ->close();
    }
}


?>

<!DOCTYPE html>

<head>
	<title>Dev Your Team</title>
	<link rel="stylesheet" id="stylesheet" href="styles/styles.css"  type="text/css" media="screen" />
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
	<div id="header">
		<div id="header_inside">
        <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
			<ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
                <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
				<li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
				<li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
				<li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
				<li><a href="index.php"   class="active" onclick="Javascript:window.location.assign('index.php');">Home</a></li>
			</ul>		
		</div>
	</div>
	<div id="content">
		<div id="content_inside">
			<div id="content_inside_sidebar"><div id="clock">&nbsp;</div>
		
				<h2>Project</h2>
				<ul>
                    <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
				</ul>
				<h2>Team</h2>
				<ul>
                    <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
				</ul>
				<h2>Ideas</h2>
				<ul>
                    <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
				</ul>
                <h2>Developers</h2>
                <ul>
                    <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                    <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
                </ul>

				<h2>Other</h2>
				<ul>
                    <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
				</ul>
			</div>
			<div id="content_inside_main">
           <?php

           if (empty($errors) === false) {
               echo '<ul>';
               foreach ($errors as $error) {
                   echo '<li class = "error">', $error, '</li>';
               }
               echo '</ul>';


               $output_form = true;
           }

           if ($output_form){

            ?>
           <form method = "post" action="<? echo $_SERVER['PHP_SELF']; ?>" onSubmit="return formCheck();">
           <table class="layout">
           <tr><td class="layout"><label for="FName">First Name: </label></td><td class="layout"><input type="text" id = "FName" name="FName" onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['FName']) === true) {echo 'value="', strip_tags($_POST['FName']), '"';} ?>></td></tr>
           <tr><td class="layout"><label for="LName">Last Name: </label></td><td class="layout"><input type="text" id = "LName" name="LName" onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['LName']) === true) {echo 'value="', strip_tags($_POST['LName']), '"';} ?>></td></tr>
           <tr><td class="layout"><label for="Email">Email: </label></td><td class="layout"><input type="text" id = "Email" name="Email" onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['Email']) === true) {echo 'value="', strip_tags($_POST['Email']), '"';} ?>></td></tr>
           <tr><td class="layout"><label for="Birthday">Birthday: </label></td><td class="layout"><input type="date" id = "Birthday" name="Birthday" onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['Birthday']) === true) {echo 'value="', strip_tags($_POST['Birthday']), '"';} ?>></td></tr>
           <tr><td class="layout"><label for="Username">Username: </label></td><td class="layout"><input type="text" id = "Username" name="Username" onChange="fieldCheck(this.id, this.value)"<?php if (isset($_POST['Username']) === true) {echo 'value="', strip_tags($_POST['Username']), '"';} ?>></td></tr>
           <tr><td class="layout"><label for="Password">Password: </label></td><td class="layout"><input type="password" id = "Password" name="Password" onChange="fieldCheck(this.id, this.value)"></td></tr>
           <tr><td class="layout"><label for="Subscribe">Subscribe to our newsletter: </label></td><td class="layout"><input type="radio" name="Subscribe" value="Yes" checked = "true">Yes <input type="radio" name="Subscribe" value="No">No </td></tr>

           </table>
           Comments:
          <textarea name = "Comments"<?php if (isset($_POST['Comments']) === true) {echo 'value="', strip_tags($_POST['Comments']), '"';} ?>></textarea> <br>
               <input type="checkbox" id = "TC" name="TC" value ="agree">I agree to the terms and conditions of registering for this website.<br>
               <input type="submit" name = 'submit' id="submit" value="Register">
           </form>
            <? }else{

               /*$data = array($FName,$LName,$Email,$Birthday,$Username,$Password);
               $data = implode("^",$data);
               $data = $data."/n";
               $filename = "data/users.txt";
               file_put_contents($filename, $data, FILE_APPEND);*/


               echo "Registered and Logged in as " . $FName . " ". $LName."<br>";

               if ($Administrator === 1){
                    echo "<input type='button' onclick='window.location(admin.php)' value='Admin'>";
               }
           }

           ?>

			</div>	
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>