<?php
    $output_form = true;

    session_start();

    if (isset($_POST['submit'])){
        $FName = filter_input(INPUT_POST, 'FName', FILTER_SANITIZE_SPECIAL_CHARS);
        $LName = filter_input(INPUT_POST, 'LName', FILTER_SANITIZE_SPECIAL_CHARS);
        $Email = filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_SPECIAL_CHARS);
        $Birthday = filter_input(INPUT_POST, 'Birthday', FILTER_SANITIZE_SPECIAL_CHARS);
        $Username = filter_input(INPUT_POST, 'Username', FILTER_SANITIZE_SPECIAL_CHARS);
        $Password = filter_input(INPUT_POST, 'Password', FILTER_SANITIZE_SPECIAL_CHARS);
        $Comments = filter_input(INPUT_POST, 'Comments', FILTER_SANITIZE_SPECIAL_CHARS);
        if (isset($_POST['administrator'])){
            $Administrator = $_POST['administrator'];
        } else {
            $Administrator = 0;
        }
        if ($Administrator === null){
            $Administrator = 0;
        }
        $id = $_POST['id'];

        $host = "localhost";
        $db_username = "agentmoobius";
        $db_password = "Moobie1234!";
        $db_name = "ICS325";

        $user_submission = array('FirstName' => $FName, 'LastName' => $LName, 'Email' => $Email, 'Birthday' => $Birthday, 'Username' => $Username, 'Password' => "$Password", 'Comments' => $Comments, 'Administrator' => $Administrator);

        $db = new mysqli($host, $db_username, $db_password, $db_name);


        $connection_error = $db->connect_error;
        if ($connection_error != null){
            $errors[] = "Errors creating database connection";
            exit;
        }

        $sql = "UPDATE users SET FirstName = '$user_submission[FirstName]', LastName =  '$user_submission[LastName]', Email = '$user_submission[Email]', Birthday = '$user_submission[Birthday]', Username = '$user_submission[Username]', Password = sha1('$user_submission[Password]'), Comments = '$user_submission[Comments]', administrator = '$user_submission[Administrator]' WHERE id = '$id';";


        $success = $db -> query($sql);
    }
?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css"  type="text/css" media="screen" />
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
        if (isset($_SESSION['loggedIn'])){

            if ($_SESSION['loggedIn']){
                if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))){
                    session_destroy();
                } else {
                    ?>
                    <div id="welcome">Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a href="logout.php" class="buttons">Logout</a>
                        <?php
                        if (isset($_SESSION['administrator'])){
                            if ($_SESSION['administrator'] == true){
                                ?>
                                <a href="admin.php" class="buttons">Admin</a>
                            <?php
                            }
                        }
                        ?>

                    </div>

                <?php
                }
            } else{
                ?>
                <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
            <?php
            }
        }else{
            ?>
            <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
        <?php
        }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php"   class="active" onclick="Javascript:window.location.assign('index.php');">Home</a></li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar"><div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            </ul>
        </div>
        <div id="content_inside_main">

            <?php



            if (empty($errors) === false) {
                echo '<ul>';
                foreach ($errors as $error) {
                    echo '<li class = "error">', $error, '</li>';
                }
                echo '</ul>';


                $output_form = true;
            }

                if($output_form){

            if (isset ($_SESSION['administrator'])){
            if ($_SESSION['administrator']){
                    if ($success){
                        echo "User info updated. <br>";
                    } else {
                        echo "User info update failed. <br>";
                    }
                echo "<a href='users.php'>View All Users</a><br>";
                echo "<a href='edit.php'>Edit User</a><br>";
                echo "<a href='add.php'>CRUD Pages</a><br>";


            }
            } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }



                }

            ?>
			</div>	
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>