<?php
session_start();
?>

<!DOCTYPE html>

<head>
	<title>Dev Your Team</title>
	<link rel="stylesheet" id="stylesheet" href="styles/styles.css"  type="text/css" media="screen" />

</head>

<body>
	<div id="header">
		<div id="header_inside">
            <?php
            if (isset($_SESSION['loggedIn'])){

                if ($_SESSION['loggedIn']){
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))){
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a href="logout.php" class="buttons">Logout</a>
                            <?php
                            if (isset($_SESSION['administrator'])){
                                if ($_SESSION['administrator'] == true){
                                    ?>
                                    <a href="admin.php" class="buttons">Admin</a>
                                <?php
                                }
                            }
                            ?>

                        </div>

                    <?php
                    }
                } else{
                    ?>
                    <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
                <?php
                }
            }else{
                ?>
                <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
            <?php
            }
            ?>
            <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
			<ul>
                <li><a href="contact.php" class="active" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
                <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
				<li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
				<li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
				<li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
				<li><a href="index.php"   onclick="Javascript:window.location.assign('index.php');">Home</a></li>
			</ul>		
		</div>
	</div>
	<div id="content">
		<div id="content_inside">
			<div id="content_inside_sidebar"><div id="clock">&nbsp;</div>
		
				<h2>Project</h2>
				<ul>
                    <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
				</ul>
				<h2>Team</h2>
				<ul>
                    <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                    <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
				</ul>
				<h2>Ideas</h2>
				<ul>
                    <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
				</ul>
                <h2>Developers</h2>
                <ul>
                    <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                    <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
                </ul>

				<h2>Other</h2>
				<ul>
                    <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
				</ul>
			</div>
			<div id="content_inside_main">



			</div>	
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>