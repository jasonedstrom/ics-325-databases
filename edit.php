<?php


    session_start();
    if (isset($_SESSION['administrator'])){


        $administrator = $_SESSION['administrator'];

        $host = "localhost";
        $db_username = "agentmoobius";
        $db_password = "Moobie1234!";
        $db_name = "ICS325";

        if (isset($_GET['id'])){
        $id = $_GET['id'];





        $db = new mysqli($host, $db_username, $db_password, $db_name);


        $connection_error = $db->connect_error;
        if ($connection_error != null){
            $errors[] = "Errors creating database connection";
            exit;
        }


        $sql = "Select * from users where id=$id;";

        $result = $db->query($sql);

        if (!$result){
            echo( "<p>Unable to query database at this time.</p>" );
            exit();

        }

        $numRows = $result ->num_rows;
        }
        $userData = null;

        if($numRows > 0){
        while ($row =  $result->fetch_assoc()){
           $userData = array('FirstName' => $row['FirstName'], 'LastName' => $row['LastName'], 'Email' => $row['Email'], 'Birthday' => $row['Birthday'], 'Username' => $row['Username'], 'Comments' => $row['Comments'], 'Administrator' => $row['administrator']);
        }
        }

        $db->close();

    }

?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css"  type="text/css" media="screen" />
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
        if (isset($_SESSION['loggedIn'])){

            if ($_SESSION['loggedIn']){
                if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))){
                    session_destroy();
                } else {
                    ?>
                    <div id="welcome">Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a href="logout.php" class="buttons">Logout</a>
                        <?php
                        if (isset($_SESSION['administrator'])){
                            if ($_SESSION['administrator'] == true){
                                $administrator = $_SESSION['administrator'];
                                ?>

                                <a href="admin.php" class="buttons">Admin</a>
                            <?php
                            }
                        }
                        ?>

                    </div>

                <?php
                }
            } else{
                ?>
                <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
            <?php
            }
        }else{
            ?>
            <div><a href="login.php" class="buttons">Login</a>   <a href="register.php" class="buttons">Register</a></div>
        <?php
        }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a></li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php"   class="active" onclick="Javascript:window.location.assign('index.php');">Home</a></li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar"><div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            </ul>
        </div>
        <div id="content_inside_main">
            <?php
              if ($administrator == true){

            ?>
            <form method = "post" action="admin.php" onSubmit="return formCheck();">
                <table class="layout">
                    <tr><td class="layout"><label for="FName">First Name: </label></td><td class="layout"><input type="text" id = "FName" name="FName" onChange="fieldCheck(this.id, this.value)" <?php if (isset($userData['FirstName']) === true) {echo 'value="', strip_tags($userData['FirstName']), '"';} ?>></td></tr>
                    <tr><td class="layout"><label for="LName">Last Name: </label></td><td class="layout"><input type="text" id = "LName" name="LName" onChange="fieldCheck(this.id, this.value)" <?php if (isset($userData['LastName']) === true) {echo 'value="', strip_tags($userData['LastName']), '"';} ?>></td></tr>
                    <tr><td class="layout"><label for="Email">Email: </label></td><td class="layout"><input type="text" id = "Email" name="Email" onChange="fieldCheck(this.id, this.value)" <?php if (isset($userData['Email']) === true) {echo 'value="', strip_tags($userData['Email']), '"';} ?>></td></tr>
                    <tr><td class="layout"><label for="Birthday">Birthday: </label></td><td class="layout"><input type="date" id = "Birthday" name="Birthday" onChange="fieldCheck(this.id, this.value)" <?php if (isset($userData['Birthday']) === true) {echo 'value="', strip_tags($userData['Birthday']), '"';} ?>></td></tr>
                    <tr><td class="layout"><label for="Username">Username: </label></td><td class="layout"><input type="text" id = "Username" name="Username" onChange="fieldCheck(this.id, this.value)"<?php if (isset($userData['Username']) === true) {echo 'value="', strip_tags($userData['Username']), '"';} ?>></td></tr>
                    <tr><td class="layout"><label for="Password">Password: </label></td><td class="layout"><input type="password" id = "Password" name="Password" onChange="fieldCheck(this.id, this.value)"></td></tr>


                </table>


                Comments:
                <textarea name = "Comments"><?php if (isset($userData['Comments']) === true) {echo strip_tags($userData['Comments']);} ?></textarea> <br>
                <label for="administrator">Administrator: </label><input type="checkbox" name="administrator" <?php if (isset($userData['Administrator']) === true) {if ($userData['Administrator']){echo "checked";}}?> value = '1'><br>
                <?php
                    echo "<input type='hidden' name='id' value='$id'>"
                ?>
                <input type="button" value="Cancel" onclick='window.location(admin.php)'>
                <input type="submit" name = 'submit' id="submit" value="Update">
            </form>
            <?php
              } else if ($_SESSION['loggedIn'] == true) {
                  echo "<script>setTimeout(function(){window.location.href = 'index.php';}, 10);</script>";
              } else{
                  echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
              }
            ?>



	    </div>
		</div>
		<div id="footer">
			<div id="footer_inside">
				<p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
			</div>
		</div>
	</div>
</body>

</html>